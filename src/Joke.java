import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Joke {

    private String setup;

    private String punchline;

    public Joke() {
        this.setup = null;
        this.punchline = null;
    }

    public String getSetup() {return setup;}
    public void setSetup(String setup) {this.setup = setup;}
    public String getPunchline() {return setup;}
    public void setPunchline(String punchline) {this.punchline = punchline;}

    public String toString() {
        return this.setup + " " + this.punchline;
    }
}
