import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLConnection;

public class HServer {

    private String url;
    private static String stuff;
    private HttpServer server;

    public HServer() {url = null;}

    public void getUrl(String string) {
        this.url = string;
    }

    public String getStuff() {return stuff;}

    public void setJsonStuff(String string) {this.stuff = string;}


    public void heyClient() throws IOException {
        server = HttpServer.create(new InetSocketAddress(8080), 0);
        HttpContext context = server.createContext("/");
        context.setHandler(HServer::sendJsonStuff);
        server.start();
    }

    public void byeClient() {
        server.stop(0);
    }

    private static void sendJsonStuff(HttpExchange ex) throws IOException {
        String blah = stuff;
        ex.sendResponseHeaders(200, blah.getBytes().length);
        OutputStream out = ex.getResponseBody();
        out.write(blah.getBytes());
        out.close();
    }

    public String getWeb() throws IOException {
        URL url = new URL(this.url);
        URLConnection connection = url.openConnection();
        BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String s = read.readLine();
        s = s.replace("[", "");
        s = s.replace("]","");
        System.out.println("Got JSON from a website. Displayed below:\n" + s);
        return s;
    }
}