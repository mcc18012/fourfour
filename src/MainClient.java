import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

public class MainClient {

    private static final String client = "http://localhost:8080/";
    private static final String url = "https://official-joke-api.appspot.com/jokes/programming/random";

    private static String TwoMonthsLater(Joke sendThis) {
        String parcel = null;
        ObjectMapper map = new ObjectMapper();
        try {
            parcel = map.writeValueAsString(sendThis);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            System.out.println("Error: Object to string conversion failed");
        }

        return parcel;
    }

    private static Joke Sort(String string) throws IOException {

        ObjectMapper map = new ObjectMapper();
        Joke sorted = map.readValue(string, Joke.class);
        return sorted;
    }


public static void main(String[] args) throws IOException {
    HServer h = new HServer();
    ObjectMapper map = new ObjectMapper();
    h.getUrl(url);
    String jString = h.getWeb();
    System.out.println(jString);
    Joke sorted =Sort(jString);
System.out.println("object to string" + jString);
String back2tf = TwoMonthsLater(sorted);
System.out.println("string back to object " + back2tf);
h.heyClient();
String sendthing = h.getWeb();
h.byeClient();
Joke jokemaker = Sort(sendthing);
System.out.println("Final result: " + jokemaker);
}
}
